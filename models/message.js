var mongoose = require('mongoose');
var userModel = require('./user');

//create Schema
var messageSchema = new mongoose.Schema({
  item: String,
  _creator : { type: String, ref: 'User' },
});

//create message model
var messageModel = mongoose.model('Message',messageSchema);

module.exports = messageModel;
