var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create Schema
var userSchema = new mongoose.Schema({
  username:String,
  password:String,
  dp:String,
  messages : [{ type: Schema.Types.ObjectId, ref: 'Message' }]
});

//create user model
var userModel = mongoose.model('User',userSchema);

module.exports = userModel;
