var React = require('react');
var createReactClass = require('create-react-class');
var ReactDOM = require('react-dom');
var axios = require('axios');

var ChatComponent = createReactClass({
  getInitialState: function(){
    return{
      user:'',
      messages: null,
    }
  },
  componentDidMount(){
    console.log('dasda');
    var self = this;
    axios.get('/all-messages')
    .then(function (response) {
      console.log(response.data);
      if(response.data != 'notexist'){
        self.setState({
          messages: response.data
        });
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  render:function(){
    return(
      <div>
        <div id="mario-chat">
          <h2>Chat Box</h2>
          <div id="chat-window">
              <div id="output"></div>
              <div id="feedback"></div>
          </div>
          <form className="" action="/post-message" method="post">
            <input id="message" type="text" name="message" placeholder="message"></input>
            <button id="send" type="submit" name="button">Send</button>
          </form>
        </div>
      </div>
    );
  }
});

module.exports = ChatComponent;
