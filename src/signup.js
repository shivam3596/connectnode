var React = require('react');
var createReactClass = require('create-react-class');

var SignupComponent = createReactClass({
  render:function(){
    return(
      <div className="register-box">
        <h2>Register</h2>
        <form className="" action="/register" method="post">
          <input className="login-input" required type="text" name="username" placeholder="username"/><br></br>
          <input className="login-input" required type="password" name="password" placeholder="password"/><br></br>
          <input type="hidden" name="dp" placeholder="password" value="none"/>
          <button className="signup-button" type="submit" name="button">Signup</button>
        </form>
        <span className="small-info">Already have an account?</span><br></br>
        <button className="signup-button login-btn" onClick={this.handleLogin} type="button" name="button">Login</button>
      </div>
    );
  },
  handleLogin:function(){
    event.preventDefault();
    this.props.handleLoginSignup(1);
  }
});

module.exports = SignupComponent;
