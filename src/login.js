var React = require('react');
var createReactClass = require('create-react-class');

var LoginComponent = createReactClass({
  render:function(){
    return(
      <div className="register-box">
        <h2>Login</h2>
        <form className="" action="/login" method="post">
          <input className="login-input" required type="text" name="username" placeholder="username"/><br></br>
          <input className="login-input" required type="password" name="password" placeholder="password"/><br></br>
          <button className="signup-button" type="submit" name="button">Login</button>
        </form>
        <span className="small-info">Do not have an account?</span><br></br>
        <button className="signup-button login-btn" onClick={this.handleSignup} type="button" name="button">Signup</button>
      </div>
    );
  },
  handleSignup:function(){
    event.preventDefault();
    this.props.handleLoginSignup(0);
  }
});

module.exports = LoginComponent;
