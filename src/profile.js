var React = require('react');
var createReactClass = require('create-react-class');
var ReactDOM = require('react-dom');
var axios = require('axios');
var ChatComponent = require('./chatcomponent');

var ProfileComponent = createReactClass({
  getInitialState: function(){
    return{
      dpUrl:null,
      image:null
    }
    this.handleFileUpload = this.handleFileUpload.bind(this);
  },
  componentDidMount(){
    var self = this;
    axios.get('/checkdp')
    .then(function (response) {
      if(response.data == 'notexist'){
        self.setState({
          dpUrl:'public/node.png'
        });
      }else{
        self.setState({
          dpUrl:'images/'+ response.data
        });
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  render:function(){
    var dpUrl = this.state.dpUrl;
    return(
      <div>
        <div className="profile">
          <h2 className="username"><b>{username}</b></h2>
          <div className="dp">
            <img className="dp" src={dpUrl} alt="dp"></img>
          </div>
          <div className="upload">
            <form onSubmit={this.handleFileUpload} >
              <div className="file-upload">
                <label htmlFor="upload" className="file-upload__label">Select</label>
                <input accept="image/*" required onChange={this.handleFile} id="upload" className="file-upload__input" type="file" name="image"></input>
                <button className="signup-button login-btn" type="submit" name="button">Upload</button>
              </div>
          </form>
          </div>
          <h2>
            <a href="/logout">Logout</a>
          </h2>
        </div>
        <div className="chatbox">
          <ChatComponent/>
        </div>
      </div>
    );
  },
  handleFile:function(e){
    this.setState({
      image:e.target.files[0]
    });
  },
  handleFileUpload:function(e){
    e.preventDefault();
    var self = this;
    var fd = new FormData();
    fd.append('image',this.state.image,this.state.image.name);
    axios.post('/fileupload',fd)
      .then(function(response){
        console.log('done');
        self.setState({
          dpUrl:'images/'+ response.data
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
});

module.exports = ProfileComponent;
