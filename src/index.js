var React = require('react');
var ReactDOM = require('react-dom');
var LoginComponent = require('./login');
var SignupComponent = require('./signup');
var ProfileComponent = require('./profile');
var createReactClass = require('create-react-class');
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

var App = createReactClass({
  render:function(){
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} component={HomeComponent}/>
          <Route path={"/profile"} component={ProfileComponent}/>
        </Switch>
      </Router>
    );
  }
});

var HomeComponent = createReactClass({
  getInitialState: function(){
    return{
      val:0
    }
  },
  render:function(){
    var val = this.state.val;
    if(val == 1){
      var comp = <LoginComponent handleLoginSignup={this.handleLoginSignup}/>;
    }else if(val == 0){
      var comp = <SignupComponent handleLoginSignup={this.handleLoginSignup}/>;
    }
    return(
      <div>
        {comp}
      </div>
    );
  },
  handleLoginSignup:function(value){
    this.setState({
      val:value
    });
  }
});

ReactDOM.render(<App/>,document.getElementById('content'));
