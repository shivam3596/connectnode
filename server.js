var express = require('express');
var socket = require('socket.io');
var session =require('express-session');
var homeController = require('./controller/homecontroller.js');
var api = require('./api/index.js');
const PORT = process.env.PORT || 8000;
var app = express();
app.set('view engine','ejs');

app.use('/public',express.static('./public'));
app.use('/images',express.static('./images'));

app.use(session({secret:'ALLISWELL',resave: true,saveUninitialized: true}));
var ssn;
api(app,ssn);

var server = app.listen(PORT);
homeController(app,ssn);
