var bodyParser = require('body-parser');
var multer = require('multer');
var fs = require('fs');
var userModel = require('../models/user.js');
var messageModel = require('../models/message.js')
var session =require('express-session');

//saving images
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'images')
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname)
    }
});

var upload = multer({storage: storage}).single('image');
var urlEncoderParser = bodyParser.urlencoded({extended:false});

module.exports = function(app,ssn){

  app.get('/',function(req,res){
    ssn = req.session;
    res.render('home',{username:'',err:ssn.error});
    req.session.destroy();
  });

  app.post('/register',urlEncoderParser,function(req,res){
    ssn = req.session;
    userModel.countDocuments({username:req.body.username},function(err,count){
      if(req.body.username.length > 25 || req.body.password.length > 20){
        ssn.error = "Pick a username/password, not an essay!";
        res.redirect('/');
      } else if(count == 0){
        var newData = userModel(req.body).save(function(err,data){
          if (err) {
            throw err;
          }else{
            ssn.username = req.body.username;
            res.redirect('/profile');
          }
        });
      }else{
        ssn.error = "Username already exists!";
        res.redirect('/');
      }
    });
  });


  app.post('/post-message',urlEncoderParser,function(req,res){
    ssn = req.session;
    userModel.find({username:ssn.username},function(err,data){
      if (req.body.message) {
        var newData = messageModel({ item: req.body.message, _creator: data[0]._id }).save(function(err,data){
          if (err) {
            throw err;
          }else{
            res.redirect('/profile');
          }
        });
      }else{
        res.send('none');
      }
    });
  });

  app.get('/profile',function(req,res){
    ssn = req.session;
    if (ssn.username) {
      res.render('home',{username:ssn.username});
    }else{
      res.render('404');
    }
  });

  app.get('/logout',function(req,res){
    req.session.destroy(function(err) {
      if(err) {
        throw err;
      } else {
        res.redirect('/');
      }
    });
  });

  app.get('/checkdp',function(req,res){
    ssn = req.session;
    userModel.find({username:ssn.username},function(err,data){
      if (fs.existsSync('images/'+ data[0].dp)) {
        res.send(data[0].dp);
      }else{
        res.send('notexist');
      }
    });
  });

  app.get('/all-messages',function(req,res){
    ssn = req.session;
    userModel.find({username:ssn.username},function(err,data){
      messageModel.find({_creator:data[0]._id},function(err,data){
        if (data[0].messages) {
          res.send(data[0].messages);
        }else{
          res.send('notexist');
        }
      });
    });
  });

  app.post('/fileupload',function(req,res,next){
    ssn = req.session;
    upload(req, res, function (err) {
      if (err) {
        throw err;
      }else{
        userModel.update({username: ssn.username}, {
          dp:req.file.originalname
        }, function(err, numberAffected, rawResponse) {
           res.send(req.file.originalname);
        });
      }
    });
  });

}
